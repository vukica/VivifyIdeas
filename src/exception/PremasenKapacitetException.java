package exception;

public class PremasenKapacitetException extends Exception{

	private static final long serialVersionUID = 1L;

	public PremasenKapacitetException() {}

	public PremasenKapacitetException(String message)
	{
		super(message);
	}
}
