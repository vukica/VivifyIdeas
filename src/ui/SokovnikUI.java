package ui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import exception.PremasenKapacitetException;
import model.Jabuka;
import model.Sokovnik;

public class SokovnikUI {
static Scanner sc = new Scanner(System.in);
	
	
	
	public static void main(String[] args) throws IOException {
		
		String sP = System.getProperty("file.separator");
		File file = new File("."+sP+"data"+sP+"izvestaj.txt");
		PrintWriter out2 = new PrintWriter(new FileWriter(file), true);
		
		Sokovnik sokovnik = Sokovnik.getInstance();

		int odluka = -1;
		while (odluka != 0) {
			System.out.println("     ---Meni---");
			System.out.println("Opcija 1 Dodaj voce u posudu");
			System.out.println("Opcija 2 Cedi voce");
			System.out.println("Opcija 3 Preostalo prostora u posudi");
			System.out.println("Opcija 4 Broj vocki u posudi");
			System.out.println("Opcija 5 Izlaz");
			
			System.out.print("opcija:");
			odluka = ocitajCeoBroj();
			
			switch (odluka) {
			case 1:
				Jabuka jabuka = new Jabuka("Macintosh");
				try {
					out2.println(sokovnik.dodajVocku(jabuka));
				} catch (PremasenKapacitetException e) {
					out2.println("Prema�en kapacitet. Ukupno dobijeno " + sokovnik.getUkupnaKolicinaSoka() + " l soka.");
					e.printStackTrace();
					System.exit(0);
				}
				break;
			case 2:
				out2.println(sokovnik.cediVoce());
				break;
			case 3:
				System.out.println("Preostalo mesta u posudi je " + sokovnik.getPosuda().preostaloProstoraUPosudi() + " kg.");
				break;
			case 4:
				System.out.println("Broj vocki u posudi je " + sokovnik.getPosuda().brojVockiUPosudi() + ".");
				break;
			case 5:
				out2.println("Zavrsen rad sokovnika. Sokovnik je imao " + Sokovnik.getBrojAkcija() + " akcija.");
				out2.println("Ukupno napravljeno " + sokovnik.getUkupnaKolicinaSoka() + " l soka.");
				System.out.println("Zavrsen rad sokovnika. Sokovnik je imao " + Sokovnik.getBrojAkcija() + " akcija.");
				System.out.println("Ukupno napravljeno " + sokovnik.getUkupnaKolicinaSoka() + " l soka.");
				System.exit(0);
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
		out2.flush();
		out2.close();
	}
		
	public static int ocitajCeoBroj(){
		while (sc.hasNextInt() == false) {
			System.out.println("Pogresno uneta vrednost, pokusajte ponovo: ");
			sc.nextLine();
		}
		int ceoBroj = sc.nextInt();
		sc.nextLine(); 
		return ceoBroj;
	}
}
