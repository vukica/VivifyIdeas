package model;

import java.util.Random;

public class Jabuka extends Voce{
private String sorta;
	
	public Jabuka() {
		super("Jabuka", generisiTezinu(),(Math.random() < 0.2 ) ? true : false);
	}
	public Jabuka(String sorta) {
		super("Jabuka", generisiTezinu(),(Math.random() < 0.2 ) ? true : false);
		this.sorta = sorta;
	}
	
	private static double generisiTezinu() {
		int rangeMax = 3;
		int rangeMin = 1;
		Random r = new Random();
		double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
		return randomValue;
	}
	@Override
	public String toString() {
		if(sorta == null) {
			return super.toString() + "Sorta: " + "nije navedena";
		}else {
			return super.toString() + "Sorta: " + sorta;	
		}
	}

}
