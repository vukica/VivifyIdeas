package model;

import exception.PremasenKapacitetException;

public class Sokovnik implements Cediljka{
	private static int brojAkcija = 0;
	private Posuda posuda = new Posuda();
	private double ukupnaKolicinaSoka = 0;

	public static int getBrojAkcija() {
		return brojAkcija;
	}
	public Posuda getPosuda() {
		return posuda;
	}

	public double getUkupnaKolicinaSoka() {
		return ukupnaKolicinaSoka;
	}

	private Sokovnik() {}

	private static Sokovnik instance;

	public static Sokovnik getInstance() {
		if(instance == null) {
			instance = new Sokovnik();
		}
		return instance;
	}

	@Override
	public String cediVoce() {
		double kolicinaSoka = 0;
		if(brojAkcija > 100) {
			System.out.println("Sokovnik je izvrsio " + brojAkcija + " akcija. Zavrsen rad sokovnika.");
			System.exit(0);
		}
		
		if(verovatnocaCedjenja()) {
			kolicinaSoka = 0.4 * posuda.popunjenostPosude();
			posuda.getSadrzaj().clear();
			brojAkcija ++;
			ukupnaKolicinaSoka += kolicinaSoka;
			System.out.println("Voce je uspesno iscedjeno. Dobijeno je " + kolicinaSoka + " l soka.");
			return "Voce je uspesno iscedjeno. Dobijeno je " + kolicinaSoka + " l soka.";
		}else {
			System.out.println("Voce nije iscedjeno.");
			return "Voce nije iscedjeno.";
		}
	}
	public String dodajVocku(Voce voce) throws PremasenKapacitetException {
		if(! voce.isTrula()) {
			Jabuka jabuka = (Jabuka)voce; 
			if(verovatnocaDodavanja()) {
				posuda.dodajVocku(jabuka);
				System.out.println(jabuka + " je uspesno dodata.");
				brojAkcija ++;
				if(premasenKapacitet()) {
					throw new PremasenKapacitetException("Premasen kapacitet.");	
				}
				return "Uspesno dodato voce " + jabuka;
			}else {
				System.out.println(voce.getNaziv() + " nije dodata.");
				return voce.getNaziv() + " nije dodata.";
			}
		}else {
			System.out.println("Trula vocka nije dodata.");
			return "Trula vocka nije dodata.";
		}

	}
	boolean verovatnocaCedjenja() {
		double verovatnoca = Math.random();
		return verovatnoca <= 0.3;
	}
	boolean verovatnocaDodavanja() {
		double verovatnoca = Math.random();
		return verovatnoca <= 0.7;
	}
	boolean premasenKapacitet() {
		return (posuda.popunjenostPosude() > 30);	
	}

}
