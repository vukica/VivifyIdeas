package model;

public class Voce {
	private String naziv;
	private double tezina;
	private boolean trula;
	
	
	public Voce() {
		super();
	}
	
	public Voce(String naziv, double tezina, boolean trula) {
		super();
		this.naziv = naziv;
		this.tezina = tezina;
		this.trula = trula;
	}

	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public double getTezina() {
		return tezina;
	}
	public void setTezina(double tezina) {
		this.tezina = tezina;
	}
	
	public boolean isTrula() {
		return trula;
	}

	public void setTrula(boolean trula) {
		this.trula = trula;
	}

	@Override
	public String toString() {
		return "Naziv: " + naziv + "; Tezina: " + tezina + "kg; " + "Trula: " + trula + "; ";
	}
	
}
