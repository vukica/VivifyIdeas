package model;

import java.util.ArrayList;

public class Posuda {
	private double kapacitet = 30;
	private ArrayList<Voce> sadrzaj = new ArrayList<>();
	
	public ArrayList<Voce> getSadrzaj() {
		return sadrzaj;
	}
	public void setSadrzaj(ArrayList<Voce> sadrzaj) {
		this.sadrzaj = sadrzaj;
	}
	public int brojVockiUPosudi() {
		return sadrzaj.size();
	}
	public double popunjenostPosude() {
		double popunjenost = 0;
		if(sadrzaj.size() == 0) {
			return popunjenost;
		}
		for(int i = 0; i < sadrzaj.size(); i++) {
			popunjenost += sadrzaj.get(i).getTezina();
		}
		return popunjenost;
	}
	public double preostaloProstoraUPosudi() {
		return kapacitet - popunjenostPosude();
	}
	public boolean dodajVocku(Voce voce) {
		sadrzaj.add(voce);
		return true;
	}
}
